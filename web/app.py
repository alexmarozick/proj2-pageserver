'''
Alex Marozick

This program renders html files into the browser using flask
'''

from flask import Flask, render_template

app = Flask(__name__)
    
@app.route('/<path:name>')
def hello(name):
    try:
        if(('//' in name) or name == '~' or name == '..'  or name == '.'):
            return error_403(None)   # file is forbidden
        else:
            return render_template(str(name))   # success
    
    except:   # file not found
        return error_404(None)
        
    
@app.errorhandler(404)
def error_404(error):
    #return a template with error code which is "File not found"
    return render_template("404.html"), 404


@app.errorhandler(403)
def error_403(error):
    #return a template with error code which is "File is forbidden!"
    return render_template("403.html"), 403
    

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
